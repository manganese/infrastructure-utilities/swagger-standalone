const fs = require('fs');
const path = require('path');
const express = require('express');
const swaggerUiAssetPath = require('swagger-ui-dist').getAbsoluteFSPath();

module.exports = class Server {
  constructor(serverConfiguration, swaggerConfigurationPath) {
    this.serverConfiguration = serverConfiguration;
    this.swaggerConfigurationPath = swaggerConfigurationPath;
  }

  start() {
    const swaggerConfiguration = JSON.parse(
      fs.readFileSync(this.swaggerConfigurationPath)
    );
    const application = (this.application = express());
    application.get('/api.json', (_request, response) => {
      response.json(swaggerConfiguration);
    });
    application.use(express.static(path.join(__dirname, '../../dist')));
    application.listen(this.serverConfiguration.port);
  }
};
