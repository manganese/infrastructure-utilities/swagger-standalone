var SwaggerUIBundle = require('swagger-ui-dist').SwaggerUIBundle;

const ui = SwaggerUIBundle({
  url: '/api.json',
  dom_id: '#root',
  presets: [SwaggerUIBundle.presets.apis],
});
