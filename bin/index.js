#!/usr/bin/env node
const fs = require('fs-extra');
const path = require('path');
const Server = require('../src/server');

const DEFAULT_SERVER_CONFIGURATION = {
  port: 8080,
};

const yargsStartCommand = (yargs) => {
  return yargs.command(
    'start',
    'Start a new Swagger instance.',
    (yargs) => {
      yargs.option('server-config', {
        type: 'string',
        describe: 'The path to the server configuration file.',
        requiresArg: true,
      });

      yargs.option('swagger-config', {
        type: 'string',
        describe: 'The path to the Swagger documentation file.',
        requiresArg: true,
      });
    },
    async (argv) => {
      let serverConfiguration = DEFAULT_SERVER_CONFIGURATION;

      if (argv.serverConfig) {
        serverConfiguration = {
          ...serverConfiguration,
          ...(await fs.readJson(argv.serverConfig)),
        };
      }

      const server = new Server(serverConfiguration, argv.swaggerConfig);
      server.start();
    }
  );
};

const yargs = require('yargs')
  .scriptName('swagger-standalone')
  .usage('$0 <command> [arguments]');

yargsStartCommand(yargs);

yargs.help().argv;
